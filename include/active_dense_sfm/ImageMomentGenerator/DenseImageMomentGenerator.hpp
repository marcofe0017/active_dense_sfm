#ifndef DENSE_IMAGE_MOMENT_GENERATOR_HPP_
#define DENSE_IMAGE_MOMENT_GENERATOR_HPP_

// Project Header files
#include "ImageMomentGenerator.hpp"

struct weightFcnParams{

    double Kw;
    double a;
    cv::Mat wFcn; 

};


class DenseImageMomentGenerator : public ImageMomentGenerator {


public:

    /**
     * @brief Construct a new Dense Image Moment Generator object
     * 
     */
    DenseImageMomentGenerator();
    

    /**
     * @brief Construct a new Dense Image Moment Generator object
     * 
     * @param topics the list of ROS topics with which the node communicates
     * @param mom_type integer value indicating the type of moment to be used
     */
    DenseImageMomentGenerator(const std::vector < std::string >& topics, const int& mom_type = DENSE_NO_WEIGHT_MOMENT);

    /**
     * @brief Destroy the Dense Image Moment Generator object
     * 
     */
    ~DenseImageMomentGenerator();


    /**
     * @brief Read function
     * Load the parameters contained in the ROS launch file
     * 
     * @param topics the list of ROS topics with which the node communicates
     */
    void readParamsFromLaunch(const std::vector < std::string >& topics);


    /**
     * @brief Image processing function
     * 
     * Image processing function generating the image moments
     */
    void processImage();

    /**
     * @brief Init function
     * Initialize the weighting function, based on the requested input type (weightFcnType)
     */
    void initWFcn();

    /**
     * @brief Init function
     * Initialize the array of matrices defining the basis for moments of any order
     * Internally sets mombasis
     */
    void initMomBasesSet();

    /**
     * @brief Init function
     * Initialize the the single matrix defining the basis for the moment of order j+i requested in input
     * @param i the x moment order
     * @param j the y moment order
     * @param basis_ij (out) the cv::Mat object with the moment basis of order j+i
     */
    void initMomBasis(const int& i, const int& j, cv::Mat& basis_ij);

private:

    // 1: dense unweighted moment type 
    // 2: dense circle-shaped weighted moment type 
    // 3: dense ellise-shaped weighted moment type 
    int weightFcnType;                  //!< State the weighting function for the dense moments
    weightFcnParams wfParams;           //!< Struct containing the parameters related to the weighting function
    cv::Mat outputImage;                //!< Output image resulting from weight function application


    // The reason why there are two different orders for x and y is that we actually need a given set of moments (up to maxMomX and maxMomY)
    // in order to compute the weighted polynomial moment. However, for a generic moment of order i + j + 1, the corresponding dynamics depends on 
    // moments of higher orders up to (i + j + 5), that need to be properly measured as well. Therefore, we separate the two orders by storing the higher max
    // orders in maxMeasMomX and maxMeasMomY, that should be ideally equal to maxMomX + 5 and maxMomY +5, respectively
    int maxMeasMomX;                    //!< Maximum order of measured moment of x
    int maxMeasMomY;                    //!< Maximum order of measured moment of y

    std::vector < cv::Mat > mombasis;   //!< Array of matrices containing the pre-computed moment xy-basis for each order

};


#endif //DENSE_IMAGE_MOMENT_GENERATOR_HPP_
#ifndef DISCRETE_IMAGE_MOMENT_GENERATOR_HPP_
#define DISCRETE_IMAGE_MOMENT_GENERATOR_HPP_

// Project Header files
#include "ImageMomentGenerator.hpp"

// OpenCV Header files
#include <opencv2/features2d/features2d.hpp>


 //!< Structure containing all the parameters of Blob detection
struct ImgProcParams{

// cv::threshold parameters
int threshValue;
int threshType;

// cv::erode paramters
int erodeCoeffX10;

cv::RNG rng;

};


class DiscreteImageMomentGenerator : public ImageMomentGenerator{


public:

    /**
     * @brief Construct a new Discrete Image Moment Generator object
     * 
     */
    DiscreteImageMomentGenerator();

    /**
     * @brief Construct a new Discrete Image Moment Generator object
     * 
     * @param topics the list of ROS topics with which the node communicates
     */
    DiscreteImageMomentGenerator(const std::vector < std::string >& topics);

    /**
     * @brief Destroy the Discrete Image Moment Generator object
     * 
     */
    ~DiscreteImageMomentGenerator();

    /**
     * @brief Image processing function
     * 
     * Image processing function generating the image moments
     */
    void processImage();


private:

    ImgProcParams ipParams;         //!< Static instance of the structure containing all the parameters of Blob detection

};




#endif //DISCRETE_IMAGE_MOMENT_GENERATOR_HPP_
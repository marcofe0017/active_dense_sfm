#ifndef IMAGE_MOMENT_GENERATOR_HPP_
#define IMAGE_MOMENT_GENERATOR_HPP_

// Eigen Header files
#include <eigen3/Eigen/Dense>

// ROS Header files
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Float64MultiArray.h>

// OpenCV Header files
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

enum MOMENT_TYPE {DISCRETE_MOMENT,DENSE_NO_WEIGHT_MOMENT,DENSE_CIRCLE_WEIGHT_MOMENT,DENSE_ELLIPSE_WEIGHT_MOMENT};

struct imageROSstamped{

    cv::Mat image;
    ros::Time stamp;

};

struct cameraParams{

    double focus;       //!< Focal Length of the camera
    double px;          //!< x-coordinate of the camera principal point
    double py;          //!< y-coordinate of the camera principal point
    double height;      //!< Camera image height
    double width;       //!< Camera image width

};

class ImageMomentGenerator{


public:

    ros::NodeHandle nh;
    image_transport::ImageTransport it;
    image_transport::Subscriber image_sub_;
    ros::Subscriber cameraParamsSub;
    ros::Subscriber thetaCoeffsSub;
    ros::Publisher momentsPub;

    /**
     * @brief Construct a new Image Moment Generator object
     * 
     */
    ImageMomentGenerator();

    /**
     * @brief Construct a new Image Moment Generator object
     * 
     * @param topics the list of ROS topics with which the node communicates
     */
    ImageMomentGenerator(const std::vector < std::string >& topics);


    /**
     * @brief Destroy the Image Moment Generator object
     * 
     */
    ~ImageMomentGenerator(); 

    /**
     * @brief Read function
     * Load the parameters contained in the ROS launch file
     * 
     * @param topics the list of ROS topics with which the node communicates
     */
    void readParamsFromLaunch(const std::vector < std::string >& topics);

    /**
     * @brief imageCallback
     * 
     * Callback function reading the processed image where the detection has to be performed
     * @param msg ROS msg containing the input image
     */
     void imageCallback(const sensor_msgs::ImageConstPtr& msg);

    /**
     * @brief camParamsCallback
     * 
     * Callback function reading the camera intrinsic parameters
     * @param ciMsg ROS msg containing the camera intrinsic parameters
     */
     void camParamsCallback(const sensor_msgs::CameraInfo& ciMsg);

    /**
     * @brief thetaCoeffsCallback
     * 
     * Callback function reading the optimization theta coefficients
     * @param tMsg ROS msg containing the optimization theta coefficients
     */
     void thetaCoeffsCallback(const std_msgs::Float64MultiArray& tMsg);

    /**
     * @brief Main loop of the class associated to the corresponding ROS node
     * 
     * Contain the while loop running on the condition ros::ok()
     */
    void mainLoop();

    /**
     * @brief Check function
     * Check the value of the enabled flag, to trigger subsequent operations
     * @return true if the initialization procedures are completed
     * @return false otherwise
     */
    inline bool isGeneratorEnabled(){return this->enabled;}

    /**
     * @brief Image process function [virtual]
     * 
     * Image processing function generating the image moments
     */
    virtual void processImage() = 0;

protected:

    int rate;                       //!< Running rate of the associated ROS node
    bool enabled;                   //!< Flag stating if the initialization has finished and the node can start // TODO: think if using a enum with multiple state is better (are there other possible states?)

    cameraParams camParams;         //!< Structure containing the camera intrinsic parameters
    imageROSstamped inputImage;     //!< Input CV Mat in which the current image is stored
    std::string winName;            //!< Name of the opening cv window

    int maxMomX;                    //!< Maximum order of moment of x
    int maxMomY;                    //!< Maximum order of moment of y

    Eigen::VectorXd moments;        //!< Array of image photometric moments (written on topic)
    Eigen::VectorXd polymoments;    //!< Array of polynomial weighted photometric moments (written on topic)
    Eigen::VectorXd theta;          //!< Array of optimization theta coefficients (read from topic)
    
    /**
     * @brief Wait function
     * Loop over the camera intrinsic parameters request until they are not retrieved
     */
    void waitForCameraParams();

};


#endif // IMAGE_MOMENT_GENERATOR_HPP_
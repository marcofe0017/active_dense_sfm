#ifndef UTILS_HPP_
#define UTILS_HPP_

// Eigen Header files
#include <eigen3/Eigen/Dense>

/* File for utility functions*/

/**
* @brief Conversion function
* Convert the pair (row,column) in the corresponding index in a matrix with c cols
* @param c the number of columns of the matrix
* @param i the row index
* @param j the column index
* @return idx the corresponding index of the matrix entry
*/
int sub2ind(const int& c, const int& i, const int& j);

/**
* @brief Conversion function
* Convert the pair (row,column) in the corresponding index in a matrix with c cols
* @param c the number of columns of the matrix
* @param i the row index
* @param j the column index
* @return idx the corresponding index of the matrix entry
*/
void ind2sub(const int& c, const int& idx, int& i, int& j);

/**
 * @brief Conversion function
 * Convert the pair of pixel coordinates in the corresponding normalized coordinates, based on the knwon camera focus and image size
 * @param u image u-coordinate
 * @param v image v-coordinate
 * @param rows number of image rows
 * @param cols number of image columns
 * @param focus camera focal length
 * @param x (out) normalixed image x-coordinate
 * @param y (out) normalixed image x-coordinate
 */
void pxl2normd(const int& u, const int& v, const int& rows, const int& cols, const double& focus, double& x, double& y);

/**
 * @brief Conversion function
 * Convert the input vector in a matrix with given rows r and columns c
 * @param v the input vector
 * @param r the requested number of rows of the matrix
 * @param c the requested number of columns of the matrix
 * @return Eigen::MatrixXd the resulting matrix
 */
Eigen::MatrixXd reshapeV2M(const Eigen::VectorXd& v, const int& r, const int& c);

/**
 * @brief Conversion function
 * Convert the input matrix in the corresponding vector, by scanning the matrix row-wise
 * @param m the input matrix
 * @return Eigen::VectorXd the resulting vector
 */
Eigen::VectorXd reshapeM2V(const Eigen::MatrixXd& m);

#endif // UTILS_HPP_
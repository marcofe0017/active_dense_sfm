#ifndef ACTIVE_SENSING_HPP_
#define ACTIVE_SENSING_HPP_


class ActiveSensing{
    

public:


    /**
     * @brief Construct a new Active Sensing object
     * 
     */
    ActiveSensing();


    /**
     * @brief Destroy the Active Sensing object
     * 
     */
    ~ActiveSensing();
    
    
private:




};


#endif // ACTIVE_SENSING_HPP_
// System Header files
#include <iostream>
#include <signal.h>

// ROS Header files
#include <ros/ros.h>
#include <std_msgs/Bool.h>

// Global variables
ros::Publisher stopSimPub;
ros::Publisher startSimPub;

// Custom ROS shutdown routine
void mySigintHandler(int sig)
{
    // Do some custom action.
    // For example, publish a stop message to some other nodes.

    //------------ Stop simulation -------------------------------------//
    ros::Rate waitForStopSub(100);
    while(stopSimPub.getNumSubscribers() == 0){ waitForStopSub.sleep();}

    // ROS message to start simulation
    std_msgs::Bool stopSimMsg;
    stopSimMsg.data = true;

    // Publish Start simulation message
    stopSimPub.publish(stopSimMsg);

    // Spin ROS topic messages
    ros::spinOnce();
    //-------------------------------------------------------------------//


    // All the default sigint handler does is call shutdown()
    ros::shutdown();
}

int main(int argc, char** argv){

    std::cout << "This is the main of the V-REP ROS Proxy node ... " << std::endl;
    ros::init(argc, argv, "vrepROSProxyNode");
    ros::NodeHandle nh;

    // Override the default ros sigint handler.
    // This must be set after the first NodeHandle is created.
    signal(SIGINT, mySigintHandler);

    // Create the publishers to start and stop the V-REP simulation
    startSimPub = nh.advertise<std_msgs::Bool>("/startSimulation",1000);
    stopSimPub = nh.advertise<std_msgs::Bool>("/stopSimulation",1000);

    //------------ Start simulation -------------------------------------//
    ros::Rate waitForStartSub(100);
    while(startSimPub.getNumSubscribers() == 0){ waitForStartSub.sleep();}

    // ROS message to start simulation
    std_msgs::Bool startSimMsg;
    startSimMsg.data = true;

    // Publish Start simulation message
    startSimPub.publish(startSimMsg);
    //-------------------------------------------------------------------//

    ros::Rate rate(20);
    while(ros::ok()){
    
        // Spin ROS topic messages
        ros::spinOnce();

        // Sleep
        rate.sleep();
    
    }

    return 0;



}

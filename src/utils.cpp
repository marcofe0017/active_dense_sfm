// Project header files
#include "utils.hpp"

// System header files
#include <iostream>

/* File for utility functions */



/**
* @brief Conversion function
* Convert the pair (row,column) in the corresponding index in a matrix with c cols
* @param c the number of columns of the matrix
* @param i the row index
* @param j the column index
* @return idx the corresponding index of the matrix entry
*/
int sub2ind(const int& c, const int& i, const int& j) {

	return j + i * c;

}

/**
* @brief Conversion function
* Convert the pair (row,column) in the corresponding index in a matrix with c cols
* @param c the number of columns of the matrix
* @param i the row index
* @param j the column index
* @return idx the corresponding index of the matrix entry
*/
void ind2sub(const int& c, const int& idx, int& i, int& j) {

	i = idx / c;
	j = idx % c;

}


/**
 * @brief Conversion function
 * Convert the pair of pixel coordinates in the corresponding normalized coordinates, based on the knwon camera focus and image size
 * @param u image u-coordinate
 * @param v image v-coordinate
 * @param rows number of image rows
 * @param cols number of image columns
 * @param focus camera focal length
 * @param x (out) normalixed image x-coordinate
 * @param y (out) normalixed image x-coordinate
 */
void pxl2normd(const int& u, const int& v, const int& rows, const int& cols, const double& focus, double& x, double& y){

	x = (double)u / (focus) - cols / (2.0 * focus);
	y = (double)v / (focus) - rows / (2.0 * focus);

}

/**
 * @brief Conversion function
 * Convert the input vector in a matrix with given rows r and columns c
 * @param v the input vector
 * @param r the requested number of rows of the matrix
 * @param c the requested number of columns of the matrix
 * @return Eigen::MatrixXd the resulting matrix
 */
Eigen::MatrixXd reshapeV2M(const Eigen::VectorXd& v, const int& r, const int& c){

	Eigen::MatrixXd M;
	int n = v.size();

	if(n != r*c){
		std::cout << "ERROR! The size of the input vector does not coincide with the size of the requested matrix " << std::endl;
		std::exit(1);
	}

	M.setZero(r,c);

	for (int k = 0 ; k < n ; k ++){
		int i, j;
		ind2sub(c,k,i,j);
		M(i,j) = v(k);
	}

	return M;


}

/**
 * @brief Conversion function
 * Convert the input matrix in the corresponding vector, by scanning the matrix row-wise
 * @param m the input matrix
 * @return Eigen::VectorXd the resulting vector
 */
Eigen::VectorXd reshapeM2V(const Eigen::MatrixXd& m){

	Eigen::VectorXd v;

	v.setZero(m.rows() * m.cols());

	for (int i = 0 ; i < m.rows() ; i ++){

		for (int j = 0 ; j < m.cols() ; j ++){

			int idx = sub2ind(m.cols(),i,j);
			v(idx) = m(i,j);

		}
	}

	return v;
}

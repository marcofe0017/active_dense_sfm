// Project Header files
#include "ImageMomentGenerator.hpp"


/**
 * @brief Construct a new Image Moment Generator object
 * 
 */
ImageMomentGenerator::ImageMomentGenerator() : it(nh) {}


/**
 * @brief Construct a new Image Moment Generator object
 * 
 * @param topics the list of ROS topics with which the node communicates
 */
ImageMomentGenerator::ImageMomentGenerator(const std::vector < std::string >& topics) : it(nh){

  // Initially set enabled to false
  this->enabled = false;

  // Read parameters from launch file
  this->readParamsFromLaunch(topics);

  // Initialize the optimization theta coefficients with the proper size
  this->theta.setZero((this->maxMomX + 1)*(this->maxMomY + 1));

}


/**
 * @brief Destroy the Image Moment Generator object
 * 
 */
ImageMomentGenerator::~ImageMomentGenerator(){}



/**
 * @brief Read function
 * Load the parameters contained in the ROS launch file
 * 
* @param topics the list of ROS topics with which the node communicates
 */
void ImageMomentGenerator::readParamsFromLaunch(const std::vector < std::string >& topics){

    // Get the mom_type ROS param
    if(nh.getParam("imgmomgen_node_rate",rate)){
        std::cout << "Read ROS Parameter imgmomgen_node_rate = " << rate << std::endl;
    }
    else{rate = 20.0;}

    // Extract the topic name strings and initialize the corresponding publishers and subscribers
    std::cout << "num of topics: " << topics.size() << std::endl;
    for (int i = 0 ; i < topics.size() ; i ++){
        std::cout << "topics[" << i << "]: " << topics[i] << std::endl;
        if(topics[i].find("images") != std::string::npos){
            image_sub_ = it.subscribe(topics[i],1, &ImageMomentGenerator::imageCallback, this);
        }
        if(topics[i].find("intrinsic") != std::string::npos){
            cameraParamsSub = nh.subscribe(topics[i],1, &ImageMomentGenerator::camParamsCallback, this);
        }
        if(topics[i].find("theta") != std::string::npos){
            thetaCoeffsSub = nh.subscribe(topics[i],1, &ImageMomentGenerator::camParamsCallback, this);
        }
        if(topics[i].find("photometric") != std::string::npos){
            momentsPub = nh.advertise<std_msgs::Float64MultiArray>(topics[i],1000);
        }
    }
    if(nh.getParam("maxMomX",this->maxMomX)){
        std::cout << "Read ROS Parameter maxMomX = " << this->maxMomX << std::endl;
    }
    else{
        this->maxMomX = 2;
    }
    if(nh.getParam("maxMomY",this->maxMomY)){
        std::cout << "Read ROS Parameter maxMomY = " << this->maxMomY << std::endl;
    }
    else{
        this->maxMomY = 2;
    }


}


/**
 * @brief Wait function
 * Loop over the camera intrinsic parameters request until they are not retrieved
 */
void ImageMomentGenerator::waitForCameraParams(){

    // Wait for the correct camera parameters to be acquired
    ros::Rate nodeRate(this->rate);
    
    camParams.focus = -1;
    while(ros::ok() && camParams.focus == -1){
      ros::spinOnce();
      nodeRate.sleep();      
    }

    std::cout << "Camera parameters acquired: " << std::endl;
    std::cout << "camera focus: " << camParams.focus << std::endl;
    std::cout << "camera px: " << camParams.px << std::endl;
    std::cout << "camera py: " << camParams.py << std::endl;

}


/**
 * @brief Main loop of the class associated to the corresponding ROS node
 * 
 * Contain the while loop running on the condition ros::ok()
 */
void ImageMomentGenerator::mainLoop(){


    // Define time variables/structures
    ros::Time t     = ros::Time::now();
    ros::Time tprev = t;
    ros::Rate nodeRate(rate);

    // Launch the while loop
    while(ros::ok()){

      ros::spinOnce();
      nodeRate.sleep();

      // [DEBUG] Measure the current time
      t = ros::Time::now();
      ros::Duration dt = ros::Time::now() - tprev;

      // Refresh time structures
      tprev = t;

      // Print time data
      //std::cout << "ROS Time = " << ros::Time::now() << std::endl;
      //std::cout << "ROS Node rate = " << 1.0/(dt.toSec()) << " Hz" << std::endl; 

    }




}



/**
 * @brief imageCallback
 * 
 * Callback function reading the processed image where the detection has to be performed
 * @param msg ROS msg containing the input image
 */
void ImageMomentGenerator::imageCallback(const sensor_msgs::ImageConstPtr& msg){

    // Define a bridge structure to store ROS buffer in OpenCV mat
    cv_bridge::CvImagePtr cv_ptr;

    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    // Copy message to the image of the display
    cv_ptr->image.copyTo(inputImage.image);
    cv::flip(inputImage.image,inputImage.image,0);

    // Store the timestamp (for refreshness check data)
    inputImage.stamp = cv_ptr->header.stamp;


    // process image function - specific for each class
    processImage();


}

/**
 * @brief thetaCoeffsCallback
 * 
 * Callback function reading the optimization theta coefficients
 * @param tMsg ROS msg containing the optimization theta coefficients
 */
void ImageMomentGenerator::thetaCoeffsCallback(const std_msgs::Float64MultiArray& tMsg){

  // Get the input size
  int N = tMsg.data.size();

  for (int i = 0 ; i < N ; i ++){
    this->theta(i) = tMsg.data[i];
  }


}



/**
 * @brief camParamsCallback
 * 
 * Callback function reading the camera intrinsic parameters
 * @param ciMsg ROS msg containing the camera intrinsic parameters
 */
void ImageMomentGenerator::camParamsCallback(const sensor_msgs::CameraInfo& ciMsg){

  // Fill the cameraParams structure
  camParams.focus  = ciMsg.K[0];
  camParams.px     = ciMsg.K[2];
  camParams.py     = ciMsg.K[5];
  camParams.height = ciMsg.height;
  camParams.width  = ciMsg.width;

}
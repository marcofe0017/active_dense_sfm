// Project Header files
#include "DiscreteImageMomentGenerator.hpp"


/**
 * @brief Construct a new Discrete Image Moment Generator object
 * 
 */
DiscreteImageMomentGenerator::DiscreteImageMomentGenerator() {}


/**
 * @brief Construct a new Discrete Image Moment Generator object
 * 
 * @param topics the list of ROS topics with which the node communicates
 */
DiscreteImageMomentGenerator::DiscreteImageMomentGenerator(const std::vector < std::string >& topics)
: ImageMomentGenerator(topics) {

    // Set the name of the OpenCV window
    winName = std::string("DiscreteImageMoment-View");
    cv::namedWindow(winName.c_str());

    // Init random seed
    ipParams.rng = cv::RNG(1234);

    if(nh.getParam("ThresholdType",ipParams.threshType)){
        std::cout << "Read ROS Parameter ThresholdType = " << ipParams.threshType << std::endl;
    }
    else{ipParams.threshType = 0; }

    if(nh.getParam("ThresholdValue",ipParams.threshValue)){
        std::cout << "Read ROS Parameter threshValue = " << ipParams.threshValue << std::endl;
    }
    else{ipParams.threshValue = 0; }


    if(nh.getParam("erodeCoeffX10",ipParams.erodeCoeffX10)){
        std::cout << "Read ROS Parameter erodeCoeffX10 = " << ipParams.erodeCoeffX10 << std::endl;
    }
    else{ipParams.erodeCoeffX10 = 5; }

    // Initialize trackbars
    cv::createTrackbar("ThresholdType",winName.c_str(),&ipParams.threshType,4);
    cv::createTrackbar("threshold",winName.c_str(),&ipParams.threshValue,255);
    cv::createTrackbar("erode",winName.c_str(),&ipParams.erodeCoeffX10,200);

}

/**
 * @brief Destroy the Discrete Image Moment Generator object
 * 
 */
DiscreteImageMomentGenerator::~DiscreteImageMomentGenerator(){}



/**
 * @brief Image processing function
 * 
 * Image processing function generating the image moments
 */
void DiscreteImageMomentGenerator::processImage(){

    // Show initial image
    //cv::imshow("Discrete image",inputImage);
    //cv::waitKey(30);

    // Intermediate cv Mats
    cv::Mat wrkImg, threshImg, contourImg;
 
    // Convert to gray
    cv::cvtColor(inputImage.image,wrkImg,CV_BGR2GRAY);
    
    // Apply threshold
    cv::threshold(wrkImg,threshImg,ipParams.threshValue,255,ipParams.threshType);

    // Erode operator
    if(ipParams.erodeCoeffX10 < 10) ipParams.erodeCoeffX10 = 10;
    cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size((double)(ipParams.erodeCoeffX10)*0.1,(double)(ipParams.erodeCoeffX10)*0.1));
    cv::erode(threshImg,threshImg,element);

    // Extract contours
    std::vector < std::vector < cv::Point > > contours;
    std::vector < cv::Vec4i > hierarchy;
    threshImg.copyTo(contourImg);
    cv::findContours(contourImg,contours,hierarchy,cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
    cv::cvtColor(threshImg,threshImg,CV_GRAY2BGR);
    for( size_t i = 0; i< contours.size(); i++ )
    {
        cv::Scalar color = cv::Scalar( ipParams.rng.uniform(0, 256), ipParams.rng.uniform(0,256), ipParams.rng.uniform(0,256) );
        //cv::drawContours( threshImg, contours, (int)i, color, 2, cv::LINE_8, hierarchy, 0 );
    }

    // Show the image
    cv::imshow(winName.c_str(),threshImg);
    cv::waitKey(30);//*/
}



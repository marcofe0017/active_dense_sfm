// System header files
#include <iostream>
#include <string>

// Project Header files
#include "DiscreteImageMomentGenerator.hpp"
#include "DenseImageMomentGenerator.hpp"
#include "std_msgs/Bool.h"


int main(int argc, char** argv){

    std::cout << "This is the main of the ImageMomentGenerator ROS node ... " << std::endl;
    ros::init(argc, argv, "ImageMomentGenerator");

    // Get moment type from input argument
    int mom_type = -1;
    if(argc >= 2){
        mom_type = std::stod(argv[1]);    
    }
    else{
        mom_type = DENSE_CIRCLE_WEIGHT_MOMENT;
    }

    // Collect input arguments as ROS topics
    std::vector < std::string > topics_;
    for (int i = 0 ; i < argc-2; i++){

        topics_.push_back(argv[i+2]);

    }

    // Define a dynamic pointer to a generic interface
    ImageMomentGenerator *momGenPtr;


    // Create the corresponding class instance based on the acquired input argument
    if(mom_type == DISCRETE_MOMENT){

            momGenPtr = new DiscreteImageMomentGenerator(topics_);

    }
    else{

           momGenPtr = new DenseImageMomentGenerator(topics_,mom_type);

    }

    // Launch the main loop of the class as a ROS routine
    momGenPtr->mainLoop();

    // Clear dynamic data
    delete momGenPtr;


    return 0;



}

// Project Header files
#include "DenseImageMomentGenerator.hpp"
#include "utils.hpp"

/**
 * @brief Construct a new Dense Image Moment Generator object
 * 
 */
DenseImageMomentGenerator::DenseImageMomentGenerator(){}


/**
 * @brief Construct a new Dense Image Moment Generator object
 * 
 * @param topics the list of ROS topics with which the node communicates
 * @param mom_type integer value indicating the type of moment to be used
 */
DenseImageMomentGenerator::DenseImageMomentGenerator(const std::vector < std::string >& topics, const int& mom_type)
: ImageMomentGenerator(topics) {

    // Assign the moment type 
    this->weightFcnType = mom_type;

    // Read the weight function flag from the ROS launch file
    this->readParamsFromLaunch(topics);
    
    // Wait to retrieve the camera parameters
    this->waitForCameraParams();

    // Initialize the weighting function cv::Mat based on the input read flag
    this->initWFcn();

    // Initialize the array of matrices required as pre-computed bases of the moments of any order
    this->initMomBasesSet();


}

/**
 * @brief Destroy the Dense Image Moment Generator object
 * 
 */
DenseImageMomentGenerator::~DenseImageMomentGenerator(){}


/**
 * @brief Read function
 * Load the parameters contained in the ROS launch file
 * 
 * @param topics the list of ROS topics with which the node communicates
 */
void DenseImageMomentGenerator::readParamsFromLaunch(const std::vector < std::string >& topics){

    // Call the father method first
    ImageMomentGenerator::readParamsFromLaunch(topics);

    // Execute the residual calls
    if(nh.getParam("Kw",this->wfParams.Kw)){
        std::cout << "Read ROS Parameter wfParams.Kw = " << this->wfParams.Kw << std::endl;
    }
    else{
        this->wfParams.Kw = 1.0;
    }
    if(nh.getParam("a",this->wfParams.a)){
        std::cout << "Read ROS Parameter wfParams.a = " << this->wfParams.a << std::endl;
    }
    else{
        this->wfParams.Kw = 1.0;
    }

    // Given the requested max moment orders, compute the corresponding max measured moments order (see .hpp for details)
    this->maxMeasMomX = this->maxMomX + 5;
    this->maxMeasMomY = this->maxMomY + 5;

    // Initialize the moment vector with the proper size
    this->moments.setZero((this->maxMeasMomX + 1)*(this->maxMeasMomY + 1));

}

/**
 * @brief Init function
 * Initialize the weighting function, based on the requested input type (weightFcnType)
 */
void DenseImageMomentGenerator::initWFcn(){

    double x,y;
    wfParams.wFcn = cv::Mat(camParams.height,camParams.width,CV_32FC1);
    this->outputImage = cv::Mat(camParams.height,camParams.width,CV_32FC1);

    for (int i = 0 ; i < camParams.height ; i ++){


        for (int j = 0 ; j < camParams.width ; j ++){

            // Convert the pixel coordinates to the corresponding normalized coordinates
            pxl2normd(j,i,camParams.height,camParams.width,camParams.focus,x,y);

            // Assign the weighting function based on the chosen flag
            if(this->weightFcnType == DENSE_NO_WEIGHT_MOMENT){
                wfParams.wFcn.at<float>(i,j) = 1.0;
            }
            if(this->weightFcnType == DENSE_CIRCLE_WEIGHT_MOMENT){
                wfParams.wFcn.at<float>(i,j) = wfParams.Kw * exp(-wfParams.a * (pow(x,2) + pow(y,2)));
            }
            else if(this->weightFcnType == DENSE_ELLIPSE_WEIGHT_MOMENT){
                //todo
            }

        }
    }


    double minVal, maxVal;
    cv::Point minLoc, maxLoc;
    cv::minMaxLoc( this->wfParams.wFcn, &minVal, &maxVal, &minLoc, &maxLoc );

    std::cout << "maxVal of wFcn: " << maxVal << std::endl;
    std::cout << "minVal of wFcn: " << minVal << std::endl;

    // Set the enabled flag to true
    this->enabled = true;

}


/**
 * @brief Init function
 * Initialize the array of matrices defining the basis for moments of any order
 * Internally sets mombasis
 */
void DenseImageMomentGenerator::initMomBasesSet(){

    int p = (this->maxMeasMomX+1);
    int q = (this->maxMeasMomY+1);
    int arrsize = p*q;

    // Note that the indexes of x and y moment orders in the moment bases array are stored by considering the order on y moving on the columns,
    // while the orders on x moves along the rows. Therefore, here in the code the i index is the row index denoting the x-order, while the j index
    // is the column index denoting the y order of the moment basis
    for(int k = 0 ; k < arrsize ; k++){

        int i, j;
        cv::Mat basis_ij;

        // Extract the corresponding (row,col) subscripts from the input index k
        ind2sub(q,k,i,j);

        // Compute the corresponding moment basis matrix
        this->initMomBasis(i,j,basis_ij);

        // Add the computed matrix to the array of moment bases
        this->mombasis.push_back(basis_ij);


    }

}

/**
 * @brief Init function
 * Initialize the the single matrix defining the basis for the moment of order j+i requested in input
 * @param i the x moment order
 * @param j the y moment order
 * @param basis_ij (out) the cv::Mat object with the moment basis of order j+i
 */
void DenseImageMomentGenerator::initMomBasis(const int& i, const int& j, cv::Mat& basis_ij){

    basis_ij = cv::Mat(camParams.height,camParams.width,CV_32FC1);
    double x, y, diff;

    // Set the infinitesimal differential
    diff = 1.0 / (camParams.focus);

    for (int m = 0 ; m < camParams.height ; m ++){

        for (int n = 0 ; n < camParams.width ; n ++){

            // Convert from pixel coordinates to normalized coordinates
            pxl2normd(n,m,camParams.height,camParams.width,camParams.focus,x,y);

            // Assign the given basis value in the given basis
            basis_ij.at<float>(m,n) = diff * diff * pow(x,i) * pow(y,j);


        }
    }


}

/**
 * @brief Image processing function
 * 
 * Image processing function generating the image moments
 */
void DenseImageMomentGenerator::processImage(){

    cv::Mat input32F, output32F, grayInputImage;

    if(this->isGeneratorEnabled()){

        // First convert the input image from color to grayscale
        cv::cvtColor(this->inputImage.image,grayInputImage,CV_BGR2GRAY);

        // Convert the resulting image from unsigned char (8U) to single-floating (32F)
        grayInputImage.convertTo(input32F,CV_32F);

        // Apply the weight map to the converted image
        output32F = input32F.mul(this->wfParams.wFcn);

        // Finally convert back the resulting weighted image from single-floating (32F) to unsigned char (8U)
        output32F.convertTo(this->outputImage,CV_8U);

        // Prepare moment message for sending
        std_msgs::Float64MultiArray momMsg;

        // compute the moments from the resulting single-floating image and the pre-computed moment bases
        for (int k = 0 ; k < moments.size() ; k ++){

            this->moments(k) = cv::sum(output32F.mul(this->mombasis[k]))[0];

            // Assign the computed moment in the ROS message for publishing
            momMsg.data.push_back(this->moments(k));

        }

        // Compute the weighted polynomial moments with the current theta coefficients

        // Publish the moments ROS message on the chosen topic
        momentsPub.publish(momMsg);
        
    // [DEBUG]
#ifdef DEBUG
        std::cout << "Moments: " << this->moments.transpose() << std::endl;
        cv::imshow("output weighted image",this->outputImage);
        cv::waitKey(30);
#endif // DEBUG

    }


    
}
